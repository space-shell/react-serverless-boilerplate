const path = require('path')
const slsw  = require('erverless-webpack')
const nodeExternals = require('webpack-node-externals')

const sauce = path.resolve(__dirname, "src")

module.exports = {
  entry: slsw.lib.entries,
  target: "node",
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        include: __dirname,
        exclude: /node_modules/
      }
    ]
  }
}
